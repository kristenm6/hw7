// Kristen Masada
// CS2401 Fall 2015 Project
// Othello.cc

// This file contains the implementations of the functions declared in Othello.h.

#include<iostream>
#include<sstream>
#include "Othello.h"
#include "colors.h"
using namespace std;

namespace main_savitch_14 {
    game::who Othello::winning() const {
        // count number of black and white pieces on board
        int black=0, white=0;
        for (int i=0; i<8; i++) {
            for (int j=0; j<8; j++) {
                if(board[i][j].is_black()) {
                    black++;
                }
                else if(board[i][j].is_white()) {
                    white++;
                }
            }
        }
        
        // if more black pieces than white, HUMAN wins
        if (black>white) {
            return HUMAN;
        }
        // if more white pieces than black, COMPUTER wins
        else if(black<white) {
            return COMPUTER;
        }
        // if equal number of pieces of each color, it is a tie
        else {
            return NEUTRAL;
        }
    }
    
    int Othello::evaluate() const {
        int black=0, white=0;
        
        // check corners (worth value of 10 pieces)
        for (int i=0; i<8; i+=7) {
            for (int j=0; j<8; j+=7) {
                if (board[i][j].is_white()) {
                    white+=10;
                }
                else if (board[i][j].is_black()) {
                    black+=10;
                }
            }
        }
        
        // check horizontal edges (worth value of 5 pieces)
        for (int i=0; i<8; i+=7) {
            for (int j=0; j<8; j++) {
                if (board[i][j].is_white()) {
                    white+=5;
                }
                else if (board[i][j].is_black()) {
                    black+=5;
                }
            }
        }
        
        // check vertical edges (worth value of 5 pieces)
        for (int i=0; i<8; i++) {
            for (int j=0; j<8; j+=7) {
                if (board[i][j].is_white()) {
                    white+=5;
                }
                else if (board[i][j].is_black()) {
                    black+=5;
                }
            }
        }
        
        // check all pieces on board
        for (int i=0; i<8; i++) {
            for (int j=0; j<8; j++) {
                if (board[i][j].is_white()) {
                    white++;
                }
                else if (board[i][j].is_black()) {
                    black++;
                }
            }
        }
        return white-black;
    }
    
    void Othello::compute_moves(std::queue<std::string>& moves) {
        stringstream move;
        
        // clear number of passes for every two turns
        // (checks if more than 1 pass is made consecutively)
        if (moves_completed()==(pass_move_number+2)) {
            passes=0;
            pass_move_number=0;
        }
        
        // find all legal moves during a given turn
        for (int i=1; i<=8; i++) {
            for (int j=1; j<=8; j++) {
                move.str("");
                move<<i<<j;
                if (is_legal(move.str())) {
                    moves.push(move.str());
                }
            }
        }
        
        // if no legal moves available, indicate as pass
        if (moves.empty()) {
            passes++;
            pass_move_number=moves_completed();
        }
        
        return;
    }
    
    bool Othello::is_game_over() const {
        // game ends if no legal moves are available for two turns in a row
        if (passes==2) {
            return true;
        }
        
        // game ends if no empty spaces left on board
        for (int i=0; i<8; i++) {
            for (int j=0; j<8; j++) {
                if(board[i][j].is_empty()) {
                    return false;
                }
            }
        }
        return true;
    }
    
    void Othello::restart() {
        // clear board of all pieces
        for (int i=0; i<8; i++) {
            for (int j=0; j<8; j++) {
                board[i][j].clear_space();
            }
        }
        
        // set initial four pieces in middle of board
        board[3][3].set_white();
        board[3][4].set_black();
        board[4][3].set_black();
        board[4][4].set_white();
        
        // clear number of moves
        restart_moves();
        
        return;
    }
    
    void Othello::make_move(const std::string& move) {
        int x=int(move[0]-'1');
        int y=int(move[1]-'1');
        
            if (next_mover()==HUMAN) {
                // place piece on board based on user move
                board[x][y].set_black();
                
                // check spaces above to flip additional pieces
                for (int above=1; (x-above)>=0; above++) {
                    if (board[x-above][y].is_empty()) {
                        break;
                    }
                    else if(above==1&&board[x-above][y].is_black()) {
                        break;
                    }
                    else if (above>1&&board[x-above][y].is_black()) {
                        --above;
                        while ((x-above)<x) {
                            board[x-above][y].set_black();
                            --above;
                        }
                        break;
                    }
                }
                // check spaces below to flip additional pieces
                for (int below=1; (x+below)<8; below++) {
                    if (board[x+below][y].is_empty()) {
                        break;
                    }
                    else if (below==1&&board[x+below][y].is_black()) {
                        break;
                    }
                    else if (below>1&&board[x+below][y].is_black()) {
                        --below;
                        while ((x+below)>x) {
                            board[x+below][y].set_black();
                            --below;
                        }
                        break;
                    }
                }
                // check spaces to right to flip additional pieces
                for (int right=1; (y+right)<8; right++) {
                    if (board[x][y+right].is_empty()) {
                        break;
                    }
                    else if (right==1&&board[x][y+right].is_black()) {
                        break;
                    }
                    else if (right>1&&board[x][y+right].is_black()) {
                        --right;
                        while ((y+right)>y) {
                            board[x][y+right].set_black();
                            --right;
                        }
                        break;
                    }
                }
                // check spaces to left to flip additional pieces
                for (int left=1; (y-left)>=0; left++) {
                    if (board[x][y-left].is_empty()) {
                        break;
                    }
                    else if (left==1&&board[x][y-left].is_black()) {
                        break;
                    }
                    else if (left>1&&board[x][y-left].is_black()) {
                        --left;
                        while ((y-left)<y) {
                            board[x][y-left].set_black();
                            --left;
                        }
                        break;
                    }
                }
                // check spaces along diagonal w/ positive slope to flip additional pieces
                for (int pdiagonal=1; (x+pdiagonal)<8&&(y-pdiagonal)>=0; pdiagonal++) {
                    if (board[x+pdiagonal][y-pdiagonal].is_empty()) {
                        break;
                    }
                    else if (pdiagonal==1&&board[x+pdiagonal][y-pdiagonal].is_black()) {
                        break;
                    }
                    else if (pdiagonal>1&&board[x+pdiagonal][y-pdiagonal].is_black()) {
                        pdiagonal--;
                        while ((x+pdiagonal)>x) {
                            board[x+pdiagonal][y-pdiagonal].set_black();
                            pdiagonal--;
                        }
                        break;
                    }
                }
                for (int pdiagonal=1; (x-pdiagonal)>=0&&(y+pdiagonal)<8; pdiagonal++) {
                    if (board[x-pdiagonal][y+pdiagonal].is_empty()) {
                        break;
                    }
                    else if (pdiagonal==1&&board[x-pdiagonal][y+pdiagonal].is_black()) {
                        break;
                    }
                    else if (pdiagonal>1&&board[x-pdiagonal][y+pdiagonal].is_black()) {
                        pdiagonal--;
                        while ((x-pdiagonal)<x) {
                            board[x-pdiagonal][y+pdiagonal].set_black();
                            pdiagonal--;
                        }
                        break;
                    }
                }
                // check spaces along diagonal w/ negative slope to flip additional pieces
                for (int ndiagonal=1; (x-ndiagonal)>=0&&(y-ndiagonal)>=0; ndiagonal++) {
                    if (board[x-ndiagonal][y-ndiagonal].is_empty()) {
                        break;
                    }
                    else if (ndiagonal==1&&board[x-ndiagonal][y-ndiagonal].is_black()) {
                        break;
                    }
                    else if (ndiagonal>1&&board[x-ndiagonal][y-ndiagonal].is_black()) {
                        ndiagonal--;
                        while ((x-ndiagonal)<x) {
                            board[x-ndiagonal][y-ndiagonal].set_black();
                            ndiagonal--;
                        }
                        break;
                    }
                }
                for (int ndiagonal=1; (x+ndiagonal)<8&&(y+ndiagonal)<8; ndiagonal++) {
                    if (board[x+ndiagonal][y+ndiagonal].is_empty()) {
                        break;
                    }
                    else if (ndiagonal==1&&board[x+ndiagonal][y+ndiagonal].is_black()) {
                        break;
                    }
                    else if (ndiagonal>1&&board[x+ndiagonal][y+ndiagonal].is_black()) {
                        ndiagonal--;
                        while ((x+ndiagonal)>x) {
                            board[x+ndiagonal][y+ndiagonal].set_black();
                            ndiagonal--;
                        }
                        break;
                    }
                }
            }
            else {
                // place piece on board based on user move
                board[x][y].set_white();
                
                // check spaces above to flip additional pieces
                for (int above=1; (x-above)>=0; above++) {
                    if (board[x-above][y].is_empty()) {
                        break;
                    }
                    else if(above==1&&board[x-above][y].is_white()) {
                        break;
                    }
                    else if (above>1&&board[x-above][y].is_white()) {
                        --above;
                        while ((x-above)<x) {
                            board[x-above][y].set_white();
                            --above;
                        }
                        break;
                    }
                }
                // check spaces below to flip additional pieces
                for (int below=1; (x+below)<8; below++) {
                    if (board[x+below][y].is_empty()) {
                        break;
                    }
                    else if (below==1&&board[x+below][y].is_white()) {
                        break;
                    }
                    else if (below>1&&board[x+below][y].is_white()) {
                        --below;
                        while ((x+below)>x) {
                            board[x+below][y].set_white();
                            --below;
                        }
                        break;
                    }
                }
                // check spaces to right to flip additional pieces
                for (int right=1; (y+right)<8; right++) {
                    if (board[x][y+right].is_empty()) {
                        break;
                    }
                    else if (right==1&&board[x][y+right].is_white()) {
                        break;
                    }
                    else if (right>1&&board[x][y+right].is_white()) {
                        --right;
                        while ((y+right)>y) {
                            board[x][y+right].set_white();
                            --right;
                        }
                        break;
                    }
                }
                // check spaces to left to flip additional pieces
                for (int left=1; (y-left)>=0; left++) {
                    if (board[x][y-left].is_empty()) {
                        break;
                    }
                    else if (left==1&&board[x][y-left].is_white()) {
                        break;
                    }
                    else if (left>1&&board[x][y-left].is_white()) {
                        --left;
                        while ((y-left)<y) {
                            board[x][y-left].set_white();
                            --left;
                        }
                        break;
                    }
                }
                // check spaces along diagonal w/ positive slope to flip additional pieces
                for (int pdiagonal=1; (x+pdiagonal)<8&&(y-pdiagonal)>=0; pdiagonal++) {
                    if (board[x+pdiagonal][y-pdiagonal].is_empty()) {
                        break;
                    }
                    else if (pdiagonal==1&&board[x+pdiagonal][y-pdiagonal].is_white()) {
                        break;
                    }
                    else if (pdiagonal>1&&board[x+pdiagonal][y-pdiagonal].is_white()) {
                        pdiagonal--;
                        while ((x+pdiagonal)>x) {
                            board[x+pdiagonal][y-pdiagonal].set_white();
                            pdiagonal--;
                        }
                        break;
                    }
                }
                for (int pdiagonal=1; (x-pdiagonal)>=0&&(y+pdiagonal)<8; pdiagonal++) {
                    if (board[x-pdiagonal][y+pdiagonal].is_empty()) {
                        break;
                    }
                    else if (pdiagonal==1&&board[x-pdiagonal][y+pdiagonal].is_white()) {
                        break;
                    }
                    else if (pdiagonal>1&&board[x-pdiagonal][y+pdiagonal].is_white()) {
                        pdiagonal--;
                        while ((x-pdiagonal)<x) {
                            board[x-pdiagonal][y+pdiagonal].set_white();
                            pdiagonal--;
                        }
                        break;
                    }
                }
                // check spaces along diagonal w/ negative slope to flip additional pieces
                for (int ndiagonal=1; (x-ndiagonal)>=0&&(y-ndiagonal)>=0; ndiagonal++) {
                    if (board[x-ndiagonal][y-ndiagonal].is_empty()) {
                        break;
                    }
                    else if (ndiagonal==1&&board[x-ndiagonal][y-ndiagonal].is_white()) {
                        break;
                    }
                    else if (ndiagonal>1&&board[x-ndiagonal][y-ndiagonal].is_white()) {
                        ndiagonal--;
                        while ((x-ndiagonal)<x) {
                            board[x-ndiagonal][y-ndiagonal].set_white();
                            ndiagonal--;
                        }
                        break;
                    }
                }
                for (int ndiagonal=1; (x+ndiagonal)<8&&(y+ndiagonal)<8; ndiagonal++) {
                    if (board[x+ndiagonal][y+ndiagonal].is_empty()) {
                        break;
                    }
                    else if (ndiagonal==1&&board[x+ndiagonal][y+ndiagonal].is_white()) {
                        break;
                    }
                    else if (ndiagonal>1&&board[x+ndiagonal][y+ndiagonal].is_white()) {
                        ndiagonal--;
                        while ((x+ndiagonal)>x) {
                            board[x+ndiagonal][y+ndiagonal].set_white();
                            ndiagonal--;
                        }
                        break;
                    }
                }
            }
        
        return;
    }
    
    void Othello::display_status() const {
        // display horizontal coordinates
        cout<<"    1   2   3   4   5   6   7   8\n";
        cout<<"  "<<B_GREEN<<BLACK<<"---------------------------------"<<RESET<<endl;
        for (int i=0; i<8; i++) {
            // display vertical coordinates
            cout<<(i+1)<<" ";
            for (int j=0; j<8; j++) {
                // display empty space
                if(board[i][j].is_empty()) {
                    cout<<B_GREEN<<BLACK<<"|   "<<RESET;
                }
                // display space with black piece
                else if(board[i][j].is_black()) {
                    cout<<B_GREEN<<BLACK<<"| B "<<RESET;
                }
                // display space with white piece
                else if(board[i][j].is_white()) {
                    cout<<B_GREEN<<BLACK<<"|"<<WHITE<<" W "<<RESET;
                }
            }
            cout<<B_GREEN<<BLACK<<"|"<<B_BLACK<<"\n  "
                <<B_GREEN<<"---------------------------------"<<RESET<<endl;
        }
        cout<<endl;
    }
    
    bool Othello::is_legal(const std::string& move) const {
        int x=int(move[0]-'1');
        int y=int(move[1]-'1');
        // check if coordinates are off of board
        if (x>7||x<0||y>7||y<0) {
            return false;
        }
        
        // check if space is empty
        if (!board[x][y].is_empty()) {
            return false;
        }
        
        // if trying to place black piece on board...
        if (next_mover()==HUMAN) {
            // check if pieces can be "sandwiched" above
            for (int above=1; (x-above)>=0; above++) {
                if (board[x-above][y].is_empty()) {
                    break;
                }
                else if (above==1&&board[x-above][y].is_black()) {
                    break;
                }
                else if(above>1&&board[x-above][y].is_black()) {
                    return true;
                }
            }
            // check if pieces can be "sandwiched" below
            for (int below=1; (x+below)<8; below++) {
                if (board[x+below][y].is_empty()) {
                    break;
                }
                else if (below==1&&board[x+below][y].is_black()) {
                    break;
                }
                else if(below>1&&board[x+below][y].is_black()) {
                    return true;
                }
            }
            // check if pieces can be "sandwiched" to right
            for (int right=1; (y+right)<8; right++) {
                if (board[x][y+right].is_empty()) {
                    break;
                }
                else if (right==1&&board[x][y+right].is_black()) {
                    break;
                }
                else if(right>1&&board[x][y+right].is_black()) {
                    return true;
                }
            }
            // check if pieces can be "sandwiched" to left
            for (int left=1; (y-left)>=0; left++) {
                if (board[x][y-left].is_empty()) {
                    break;
                }
                else if (left==1&&board[x][y-left].is_black()) {
                    break;
                }
                else if(left>1&&board[x][y-left].is_black()) {
                    return true;
                }
            }
        
            // check if pieces can be "sandwiched" along diagonal w/ positive slope
            for (int pdiagonal=1; (x+pdiagonal)<8&&(y-pdiagonal)>=0; pdiagonal++) {
                if (board[x+pdiagonal][y-pdiagonal].is_empty()) {
                    break;
                }
                else if (pdiagonal==1&&board[x+pdiagonal][y-pdiagonal].is_black()) {
                    break;
                }
                else if(pdiagonal>1&&board[x+pdiagonal][y-pdiagonal].is_black()) {
                    return true;
                }
            }
            for (int pdiagonal=1; (x-pdiagonal)>=0&&(y+pdiagonal)<8; pdiagonal++) {
                if (board[x-pdiagonal][y+pdiagonal].is_empty()) {
                    break;
                }
                else if (pdiagonal==1&&board[x-pdiagonal][y+pdiagonal].is_black()) {
                    break;
                }
                else if(pdiagonal>1&&board[x-pdiagonal][y+pdiagonal].is_black()) {
                    return true;
                }
            }
            // check if pieces can be "sandwiched" along diagonal w/ negative slope
            for (int ndiagonal=1; (x+ndiagonal)<8&&(y+ndiagonal)<8; ndiagonal++) {
                if (board[x+ndiagonal][y+ndiagonal].is_empty()) {
                    break;
                }
                else if (ndiagonal==1&&board[x+ndiagonal][y+ndiagonal].is_black()) {
                    break;
                }
                else if(ndiagonal>1&&board[x+ndiagonal][y+ndiagonal].is_black()) {
                    return true;
                }
            }
            for (int ndiagonal=1; (x-ndiagonal)>=0&&(y-ndiagonal)>=0; ndiagonal++) {
                if (board[x-ndiagonal][y-ndiagonal].is_empty()) {
                    break;
                }
                else if (ndiagonal==1&&board[x-ndiagonal][y-ndiagonal].is_black()) {
                    break;
                }
                else if(ndiagonal>1&&board[x-ndiagonal][y-ndiagonal].is_black()) {
                    return true;
                }
            }
        }
        // if trying to place white piece on board...
        else {
            // check if pieces can be "sandwiched" above
            for (int above=1; (x-above)>=0; above++) {
                if (board[x-above][y].is_empty()) {
                    break;
                }
                else if (above==1&&board[x-above][y].is_white()) {
                    break;
                }
                else if(above>1&&board[x-above][y].is_white()) {
                    return true;
                }
            }
            // check if pieces can be "sandwiched" below
            for (int below=1; (x+below)<8; below++) {
                if (board[x+below][y].is_empty()) {
                    break;
                }
                else if (below==1&&board[x+below][y].is_white()) {
                    break;
                }
                else if(below>1&&board[x+below][y].is_white()) {
                    return true;
                }
            }
            // check if pieces can be "sandwiched" to right
            for (int right=1; (y+right)<8; right++) {
                if (board[x][y+right].is_empty()) {
                    break;
                }
                else if (right==1&&board[x][y+right].is_white()) {
                    break;
                }
                else if(right>1&&board[x][y+right].is_white()) {
                    return true;
                }
            }
            // check if pieces can be "sandwiched" to left
            for (int left=1; (y-left)>=0; left++) {
                if (board[x][y-left].is_empty()) {
                    break;
                }
                else if (left==1&&board[x][y-left].is_white()) {
                    break;
                }
                else if(left>1&&board[x][y-left].is_white()) {
                    return true;
                }
            }
            
            // check if pieces can be "sandwiched" along diagonal w/ positive slope
            for (int pdiagonal=1; (x+pdiagonal)<8&&(y-pdiagonal)>=0; pdiagonal++) {
                if (board[x+pdiagonal][y-pdiagonal].is_empty()) {
                    break;
                }
                else if (pdiagonal==1&&board[x+pdiagonal][y-pdiagonal].is_white()) {
                    break;
                }
                else if(pdiagonal>1&&board[x+pdiagonal][y-pdiagonal].is_white()) {
                    return true;
                }
            }
            for (int pdiagonal=1; (x-pdiagonal)>=0&&(y+pdiagonal)<8; pdiagonal++) {
                if (board[x-pdiagonal][y+pdiagonal].is_empty()) {
                    break;
                }
                else if (pdiagonal==1&&board[x-pdiagonal][y+pdiagonal].is_white()) {
                    break;
                }
                else if(pdiagonal>1&&board[x-pdiagonal][y+pdiagonal].is_white()) {
                    return true;
                }
            }
            // check if pieces can be "sandwiched" along diagonal w/ negative slope
            for (int ndiagonal=1; (x+ndiagonal)<8&&(y+ndiagonal)<8; ndiagonal++) {
                if (board[x+ndiagonal][y+ndiagonal].is_empty()) {
                    break;
                }
                else if (ndiagonal==1&&board[x+ndiagonal][y+ndiagonal].is_white()) {
                    break;
                }
                else if(ndiagonal>1&&board[x+ndiagonal][y+ndiagonal].is_white()) {
                    return true;
                }
            }
            for (int ndiagonal=1; (x-ndiagonal)>=0&&(y-ndiagonal)>=0; ndiagonal++) {
                if (board[x-ndiagonal][y-ndiagonal].is_empty()) {
                    break;
                }
                else if (ndiagonal==1&&board[x-ndiagonal][y-ndiagonal].is_white()) {
                    break;
                }
                else if(ndiagonal>1&&board[x-ndiagonal][y-ndiagonal].is_white()) {
                    return true;
                }
            }
        }
        return false;
    }
}













