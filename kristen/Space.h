// Kristen Masada
// CS2401 Fall 2015 Project
// Space.h

// This class handles a single space on an Othello board.

#ifndef Space_h
#define Space_h
namespace main_savitch_14 {
    class Space {
    public:
        Space(int c=-1) { chip=c; }
        void clear_space() { chip = -1; }
        bool is_empty() const { return (chip==-1); }
        void flip();
        bool is_black() const { return (chip==0); }
        void set_black() { chip=0; }
        bool is_white() const { return (chip==1); }
        void set_white() { chip=1; }
    private:
        int chip;   // empty=-1, black=0, white=1
    };
}

#endif /* Space_h */

