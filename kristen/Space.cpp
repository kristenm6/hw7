// Kristen Masada
// CS2401 Fall 2015 Project
// Space.cc

// This file contains the implementations of the functions declared in Space.h.

#include "Space.h"

namespace main_savitch_14 {
    
    void Space::flip() {
        // if chip is black, flip to white
        if(chip==0) {
            chip=1;
        }
        // if chip is white, flip to black
        else if(chip==1) {
            chip=0;
        }
    
        return;
    }

}
