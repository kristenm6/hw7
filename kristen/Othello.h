// Kristen Masada
// CS2401 Fall 2015 Project
// Othello.h

// This class refines Main and Savitch's game class to be able to play Othello.

#ifndef Othello_h
#define Othello_h

#include "game.h"
#include "Space.h"

namespace main_savitch_14 {
    class Othello: public game {
    public:
        Othello(int p=0, int pmn=0) { passes=p; pass_move_number=pmn; }
        game* clone() const { return new Othello(*this); }
        game::who winning() const;
        int evaluate() const;
        void compute_moves(std::queue<std::string>& moves);
        bool is_game_over() const;
        void restart();
        void make_move(const std::string& move);
        void display_status() const;
        bool is_legal(const std::string& move) const;
    private:
        Space board[8][8];
        int passes;     // number of times players had to consecutively
                        // pass their turns because no legal moves were available
        int pass_move_number;   // move number when a player had to pass their turn
    };
}

#endif /* Othello_h */

