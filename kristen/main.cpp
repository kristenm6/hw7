// Kristen Masada
// CS2401 Fall 2015 Project
// main.cc

#include <iostream>
#include "game.h"
#include "Othello.h"
using namespace std;
using namespace main_savitch_14;

int main() {
    Othello game;
    int winner;
    
    cout<<"Welcome to Othello!\n"
        <<"Please enter each move as a two digit number---the first of which being\n"
        <<"the row number and the second of which being the column number.\n"
        <<"For instance, if you would like to place a piece on the space at the \n"
        <<"4th row and 5th column, you would enter 45.\n\n"
        <<"NOTE: HUMAN is black and COMPUTER is white.\n\n";
    winner=game.play();
    if(winner==game::HUMAN) {
       cout<<"HUMAN won! Congratulations!\n";
    }
    else if(winner==game::COMPUTER) {
        cout<<"COMPUTER won! Congratulations!\n";
    }
    else if(winner==game::NEUTRAL) {
        cout<<"It's a tie!\n";
    }
    
    return 0;
}

