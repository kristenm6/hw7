#ifndef STATS_H_INCLUDED
#define STATS_H_INCLUDED

class Stats{

public:
Stats(){
	avg_wait=0;
	success_planes=0;
	failed_planes=0;
	alive=0;
	deaths=0;
	fires=0;
	total_planes=0;
	wait = 0;
	tlands=0;
	tpassengers=0;
}

//Mutator Functions
void set_wait(double x){ wait= wait + x;}
void set_splanes(int x){ success_planes= x + success_planes;}
void set_fplanes(int x){ failed_planes= x + failed_planes;}
void set_tlands(int x) {tlands = tlands + x;}
void set_lands(int x){ alive = x + alive;}
void set_deaths(int x){ deaths = x + deaths;}
void set_fires(int x){ fires = x + fires;}
void set_total_planes(int x){total_planes= x + total_planes;}
void set_total_passengers(int x){tpassengers = tpassengers + x;}

//Accessor Functions
double get_avg(){return avg_wait;}
int get_splanes(){return success_planes;}
int get_fplanes(){return failed_planes;}
int get_lands(){return alive;}
int get_deaths(){return deaths;}
int get_fires(){return fires;}
int get_total_planes(){return total_planes;}

//Actual statistics
double stat_avg_wait(){return wait/total_planes;}
double stat_splanes(){return success_planes/total_planes;}
double stat_fplanes(){return failed_planes/total_planes;}
double stat_fires(){return fires/total_planes;}
double passenger_deaths(){return deaths/tpassengers;}
double passenger_alive(){return alive/tpassengers;}


private:

double success_planes; //nubmer of successful lands
double failed_planes;//number of fialed lands
double alive;		//Passengers Alive
double deaths;		//Passenger Dead
double fires;		//Number of fires
double avg_wait;	//average wait time

double total_planes; //total number of planes
double tlands;		//total lands
double tpassengers; //total number of passengers
double wait;	//total wait time

};
#endif // STATS_H_INCLUDED
