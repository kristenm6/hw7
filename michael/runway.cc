#include "runway.h"

Runway::Runway(){
    landing_stage = -1;
    minutes_left_on_fire = 0;
    end_landing = false;
}

void Runway::next_minute(){

    //Resets end_landing in case it ended last time
    if(landing_stage == -1){
        end_landing = false;
        return;
    }

    if(landing_stage < 7 && landing_stage != -1){
        ++landing_stage;
    }else if(landing_stage == 7){
        landing_stage = -1;
        end_landing = true;
    }
    if(minutes_left_on_fire > 0){
        --minutes_left_on_fire;
    }
}

bool Runway::is_open(){
    if(landing_stage == -1 && minutes_left_on_fire == 0){
        return true;
    }else{
        return false;
    }
}
