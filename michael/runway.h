#ifndef RUNWAY_H_INCLUDED
#define RUNWAY_H_INCLUDED

class Runway{
    public:
        Runway();
        int get_landing_stage(){return landing_stage;}
        int get_minutes_left_on_fire(){return minutes_left_on_fire;}
        bool get_end_landing(){return end_landing;}
        void start_landing(){landing_stage = 0; end_landing = false;}
        void next_minute();
        void set_on_fire(){minutes_left_on_fire = 20;}
        void second_fire(Runway x){minutes_left_on_fire = x.minutes_left_on_fire + 20;}
        void third_fire(Runway x, Runway y){
            minutes_left_on_fire=x.minutes_left_on_fire + y.minutes_left_on_fire +20;}
        bool is_open();

    private:
        int landing_stage;          //-1 shows that the runway isn't in use
        int minutes_left_on_fire;
        bool end_landing;
};


#endif // RUNWAY_H_INCLUDED
