#include "airplane.h"
#include <cstdlib>
#include <ctime>

Airplane::Airplane(){
    people = rand() % 525;
    fuel = (rand() % 50) + 5;
    arrival_time = 0;
    time_in_queue = 0;
}

Airplane::Airplane(int f){
    fuel = f;
}

bool Airplane::operator < (const Airplane& other){
    if(fuel == other.fuel){
        return(people < other.people);
    }else{
        return(fuel < other.fuel);
    }
}
