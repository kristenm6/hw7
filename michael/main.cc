#include <iostream>
#include <cstdlib>
#include <ctime>
#include <queue>
#include <fstream>
#include "airplane.h"
#include "runway.h"
#include "stats.h"

using namespace std;

int main(int argc, char* argv[]){
//int main(){
    srand(time(NULL));

    //For Statistics
    int total_passengers = 0;
    int total_time = 0;
    int total_airplanes = 0;
    int good_landings = 0;
    int bad_landings = 0;
    int people_landed = 0;
    int dead_people = 0;

    //int minutes = 200;
    int minutes = atoi(argv[1]) * 60;

    Stats info;

    Runway runway1;
    Runway runway2;
    Runway runway3;
    Airplane* tmp;
    Airplane* tmp1;
    Airplane* tmp2;
    Airplane* tmp3;

    priority_queue<Airplane*> planes;

    ofstream events;
    ofstream stats;
    ofstream waiting;


    events.open("events.txt");


    for(int i = 0; i < minutes; ++i){

        //New Airplane
        if(i == 90 && argv[2][0] == 'y'){
            tmp = new Airplane(4);
            planes.push(tmp);
        }

        if(rand() < (0.45 * RAND_MAX)){
            tmp = new Airplane;
            try{
                if(tmp->get_people() < 5){
                    throw Bad_plane();
                }else{
                    tmp->set_arrival_time(i);
                    planes.push(tmp);
                    info.set_total_planes(1);
                    info.set_total_passengers(tmp->get_people());
                    events << "New Arrival!  Time: " << total_time << "  Fuel: " <<  tmp->get_fuel() << endl;
                }
            }catch(Bad_plane){
                cout << "Rerouting Plane" << endl;
            }
            //cout << tmp->get_fuel() << endl;
        }else{
            //cout << "X" << endl;
        }

        //Determine if any runways have caught fire
        if(rand() < (0.0005*RAND_MAX)){

            runway1.set_on_fire();
            cout<<"Runway1 has a fire!!"<<endl;
            events << "Runway 1 caught fire!  Time: " << total_time << endl;
            info.set_fires(1);
        }
            //test to see if two runways are on fire'
            if(rand() < (0.0005*RAND_MAX)){
               runway2.second_fire(runway1);
               cout<<"Runway2 has a fire!!!"<<endl;
               info.set_fires(1);
               events << "Runway 2 caught fire!  Time: " << total_time << endl;

            }
                if(rand() <(0.0005 * RAND_MAX)){
                    runway3.third_fire(runway1, runway2);
                    cout<<"Runway3 has a fire!!!"<<endl;
                    info.set_fires(1);
                    events << "Beginning landing on Runway 3!  Time: " << total_time << endl;

                }

        //Places Airplane in runway, if there is one
        if(runway1.is_open() && !planes.empty()){
            tmp1 = planes.top();
            planes.pop();
            tmp1->set_time_in_queue(i - tmp1->get_arrival_time());
            if((tmp1->get_time_in_queue() - tmp1->get_arrival_time()) < tmp1->get_fuel()){
                runway1.start_landing();
                info.set_lands(tmp1->get_people());
                info.set_splanes(1);
            }else{
                cout << "Crash" << endl;
                ++bad_landings;
                dead_people += tmp1->get_people();
            }
        }else if(runway2.is_open() && !planes.empty()){
            tmp2 = planes.top();
            planes.pop();
            tmp2->set_time_in_queue(i - tmp2->get_arrival_time());
            if((tmp2->get_time_in_queue() - tmp2->get_arrival_time()) < tmp2->get_fuel()){
                runway2.start_landing();
                info.set_lands(tmp2->get_people());
                info.set_splanes(1);
            }else{
                cout << "Crash" << endl;
                ++bad_landings;
                dead_people += tmp2->get_people();
            }
        }else if(runway3.is_open() && !planes.empty()){
            tmp3 = planes.top();
            planes.pop();
            tmp3->set_time_in_queue(i - tmp3->get_arrival_time());
            if((tmp3->get_time_in_queue() - tmp3->get_arrival_time()) < tmp3->get_fuel()){
                runway3.start_landing();
                info.set_lands(tmp3->get_people());
                info.set_splanes(1);
            }else{
                cout << "Crash" << endl;
                ++bad_landings;
                dead_people += tmp3->get_people();
            }
        }

        runway1.next_minute();
        runway2.next_minute();
        runway3.next_minute();

        if(runway1.get_landing_stage() == -1)
            events << "Runway 1 is clear.  Time: " << total_time << endl;

        if(runway2.get_landing_stage() == -1)
            events << "Runway 2 is clear.  Time: " << total_time << endl;

        if(runway3.get_landing_stage() == -1)
            events << "Runway 3 is clear.  Time: " << total_time << endl;

        total_time += 1;



        //This is just for debugging
        if(runway1.get_end_landing() == true){
            cout << "Runway 1: " << tmp1->get_fuel() << endl;
        }else if(runway2.get_end_landing() == true){
            cout << "Runway 2: " << tmp2->get_fuel() << endl;
        }else if(runway3.get_end_landing() == true){
            cout << "Runway 3: " << tmp3->get_fuel() << endl;
        }else{
            cout << "X" << endl;
        }
    }

events.close();

    stats.open("stats.txt");



    stats.close();

    waiting.open("waiting.txt");

    int count = 1;

    while(!planes.empty()){

        tmp = planes.top();
        waiting << count << ".  Time Waiting:  " << tmp->get_time_in_queue() << "  Fuel: " << tmp->get_fuel() << endl;
        planes.pop();
        ++count;

    }

    waiting.close();


    cout<<"Total Fires: TESTING! "<<info.get_fires()<<endl;
    cout<<"Total Planes: TESTING! "<<info.get_total_planes()<<endl;

    cout.precision(4);
    cout<<showpoint<<"STAT TEST: FIRES "<<info.stat_fires()<<endl;
    cout<<"STAT TEST: ALIVE PASSENGERS "<<info.passenger_alive()<<endl;
    cout<<"STAT TEST: DEAD Passengers "<<info.passenger_deaths()<<endl;
    cout<<"STAT TEST: Succesful landings "<<info.stat_splanes()<<endl;

    return 0;
}
