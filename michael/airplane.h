#ifndef AIRPLANE_H_INCLUDED
#define AIRPLANE_H_INCLUDED
#include <iostream>

using namespace std;

class Airplane{
    public:
        Airplane();
        Airplane(int f);
        bool operator < (const Airplane& other);
        int get_people(){return people;}
        int get_fuel(){return fuel;}
        int get_arrival_time(){return arrival_time;}
        int get_time_in_queue(){return time_in_queue;}
        void set_arrival_time(int time){arrival_time = time;}
        void set_time_in_queue(int time){time_in_queue = time;}

    private:
        int people;
        int fuel;
        int arrival_time;
        int time_in_queue;
};

class Bad_plane{};



#endif // AIRPLANE_H_INCLUDED
